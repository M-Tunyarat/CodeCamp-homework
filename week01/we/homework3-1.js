let fs = require('fs');

var p1 = new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', function(err,headdata) {
        if (err)
            reject(err);
        else 
            resolve(headdata);
    });
});
var p2 = new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', function(err,bodydata) {
        if (err)
            reject(err);
        else 
            resolve(bodydata);
    });
});
   
var p3 = new Promise(function (resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function(err,legdata) {
        if (err)
            reject(err);
        else 
            resolve(legdata);
    });
});
var p4 = new Promise(function (resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function(err,feetdata) {
        if (err)
            reject(err);
        else 
            resolve(feetdata);
    });
});
   

Promise.all([p1, p2, p3, p4])
.then(function(result){
    let sum = result.join("\n");
    fs.writeFile('robot.txt', sum,'utf8', function(err,data) {

    });
    console.log('All completed!: ', result); // result = ['one','two','three']
})
.catch(function(error){
    console.error("There's an error", error);
});
