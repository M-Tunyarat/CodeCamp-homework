let fs = require('fs');

function p1() {
    return new Promise(function(resolve, reject) {
    fs.readFile('head.txt', 'utf8', function(err, headdata) {
        if (err)
            reject(err);
        else
            resolve(headdata);
    });
    });
}

function p2() {
    return new Promise(function(resolve, reject) {
    fs.readFile('body.txt', 'utf8', function(err, bodydata) {
        if (err)
            reject(err);
        else
            resolve(bodydata);
    });
    });
}

function p3() {
    return new Promise(function(resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function(err, legdata) {
        if (err)
            reject(err);
        else
            resolve(legdata);
    });
    });
}

function p4() {
    return new Promise(function(resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function(err, feetdata) {
        if (err)
            reject(err);
        else
            resolve(feetdata);
    });
    });
}

async function copyFile() {
    try {
        let result = [];
        result.push(await p1())
        result.push(await p2())
        result.push(await p3())
        result.push(await p4())
        let sum = result.join("\n");
        fs.writeFile('robot.txt', sum,'utf8', function(err,data) {
            console.log('success!!!');
        });
    } catch (error) {
    console.error(error);
        }
    }
    copyFile();

