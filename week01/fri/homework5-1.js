let assert = require('assert');
let fs = require('fs');
let homework = require('./homework5-1.js');

describe('tdd lab', function() {
    describe('checkFileExist()', function() {
        it('Should have homework5-1_eye.json existed', function(done) {
            homework.checkFileExist('homework5-1_eye.json', (err,data) => {
                if (err) 
                    done(err);
                else 
                    //assert.strictEqual(data, true, "homework5-1_eyes.json should not be empty");
                    done();
            });
        });
        it('Should have homework5-1_gender.json existed', function(done) {
            homework.checkFileExist('homework5-1_gender.json', (err,data) => {
                if (err) 
                    done(err);
                else 
                    //assert.strictEqual(data, true, "homework5-1_gender.json should not be empty");
                    done();
            });
        });
        it('Should have homework5-1_friends.json existed', function(done) {
            homework.checkFileExist('homework5-1_friends.json', (err,data) => {
                if (err) 
                    done(err);
                else 
                    //assert.strictEqual(data, true, "homework5-1_friends.json should not be empty");
                    done();
            });
        });
    });
    describe('#objectKey()', function() {
        it('Should have same object key stucture as homework5-1_eye.json', function(done) {
            homework.objectKey('homework5-1_eyes.json',function(err,data){
                let check = ['brown', 'green', 'blue'];
                assert.deepEqual(Object.keys(data), check, 'keys_eyes.json should not be empty');
                done();
            });
        });
        it('Should have same object key stucture as homework5-1_gender.json', function(done) {
            homework.objectKey('homework5-1_gender.json',function(err,data){
                let check = ['male', 'female'];
                assert.deepEqual(Object.keys(data), check, "keys_gender.json should not be empty");
                done();
            });
        });
        it('Should have same object key stucture as homework5-1_friends.json', function(done) {
            homework.objectKey('homework5-1_friends.json',function(err,data){
                let check = ['_id', 'friendCount', 'taCount' , 'balance'];
                let sumfriends = (data);
                for(let i in sumfriends) {
                    assert.deepEqual(Object.keys(sumfriends[i]), check, "keys_friends.json should not be empty");
                }
                done();
            }); 
        });
    });
    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', function(done) {
           homework.userFriendCount('homework5-1_friends.json',function(err,data){
               assert.deepEqual((data).length, 23, "size of array input as 23.json should not be empty");
               done();
           });
        });
   });
   describe('#sumOfEyes()', function() {
       it('Should have sum of eye as 23', function(done) {
           homework.sumOfEyes('homework1-4.json', function(err,data) {
               let sumeye = (data);
               let brown = 0;
               let green = 0;
               let blue = 0;
               for(let i in sumeye) {
                   if(sumeye[i].eyeColor == 'brown') {
                       brown++;
                   }
                   else if(sumeye[i].eyeColor == 'green') {
                       green++;
                   }
                   else if(sumeye[i].eyeColor == 'blue') {
                       blue++;
                   }
               }
               assert.deepEqual(brown+green+blue, 23, 'size of array input as 23');
               done();
           });
       });
   });
   describe('#sumOfGender()', function(done) {
       it('Should have sum of gender as 23', function(done) {
           homework.sumOfEyes('homework1-4.json', function(err,data) {
               let sumgender = (data);
               let male = 0;
               let female = 0;
               for(let i in sumgender) {
                   if(sumgender[i].gender == 'male') {
                       male++;
                   }
                   else if(sumgender[i].gender == 'female') {
                       female++;
                   }
               }
               assert.deepEqual(female+male, 23, 'size of array input as 23');
               done();
           });
       });
   });
});