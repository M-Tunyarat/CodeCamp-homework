describe('Async Test', function() {
    describe('#myReadFile()', function() {
    it('should be equal to this object structure', function(done) {
    myReadFile("demofile1.txt",(err, data) => {
        if (err)
            done(err);
        else {
            assert.strictEqual(data.length > 0, true, "demofile1.txt should not be empty");
    done();
        }
    });
    });
    });
});