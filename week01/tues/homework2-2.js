let fields = ['id','firstname','lastname','company','salary'];
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];

let results = [];
let data = {};

for (i=0; i<employees.length; i++) {
    for (j=0; j<employees[i].length; j++) {
        data[fields[j]] = employees[i][j];
    }
    results.push(data);
    data = {};
}
console.log(results);
